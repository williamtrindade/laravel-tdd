# Use a imagem oficial do PHP na versão 7.4
FROM php:7.4-fpm

# Atualiza a lista de pacotes e instala as dependências necessárias
RUN apt-get update \
    && apt-get install -y \
        libzip-dev \
        unzip \
        libpq-dev \
    && docker-php-ext-install pdo pdo_mysql pdo_pgsql zip

# Instala o Composer globalmente
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer

# Define o diretório de trabalho como o diretório da aplicação
WORKDIR /var/www/html

# Copia os arquivos de configuração do Laravel
COPY . .

# Instala as dependências do Composer
RUN composer install --no-interaction --optimize-autoloader

# Copia o arquivo de ambiente
COPY .env.example .env

# Gera a chave de aplicativo do Laravel
RUN php artisan key:generate

# Define as permissões necessárias
RUN chown -R www-data:www-data storage bootstrap/cache

# Expondo a porta 9000 (pode ser alterada conforme necessário)
EXPOSE 9000

# Comando para iniciar o servidor PHP-FPM
CMD ["php-fpm"]
